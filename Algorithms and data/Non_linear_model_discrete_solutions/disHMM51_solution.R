##Algorithm which generates discrete filtering and smoothing
#solutions to the non-linear HMM, parameters tau = 5, sigma = 1


library(base)
library(RUnit)
library(FKF)
library(matrixStats)
library(MASS)
library(lattice)
library(pscl)
library(truncnorm)
library(smcUtils)
library(coda)
library(mvtnorm)
library(mixtools)
library(xtable)
library(chopthin)

#Q1
tau <- 5
sigma <- 1
gen <- function(T){
  X <- rep(0,T)
  Y <- X
  X[1] <- rnorm(1, sd = 1)
  Y[1] <- rnorm(1, X[1]^2/20, sd = sigma)
  for (i in 2:T){
    X[i] <- rnorm(1, 0.5*X[i-1] + 25*(X[i-1]/(1+X[i-1]^2)) + 8*cos(1.2*i), sd = tau)
    Y[i] <- rnorm(1, X[i]^2/20, sd = sigma)
  }
  list(X,Y)
}



#Generation of the new particles
T <- 512
set.seed(10001)
data <- gen(T)
Y <- data[[2]]
obs <- data[[1]]
num <- 200


## Bootstrap PF with resampling
bootstrap.PF <- function(N,K,Y){
  X.filter <- matrix(rep(0,N*K), ncol = K)
  X.filter[,1] <- rnorm(N,mean = 0, sd = 1)
  weights <- dnorm(Y[1], mean = X.filter[,1]^2/20, sd = sigma)
  X.filter[,1] <- X.filter[sample(1:N,N,replace = TRUE, prob = weights),1]
  for (i in 1:(K-1)){
    X.filter[,(i+1)] <- rnorm(N, mean = 0.5*X.filter[,i] + 25*X.filter[,i]/(1+X.filter[,i]^2) + 8*cos(1.2*(i+1)), sd = tau)
    weights <- dnorm(Y[i+1], mean = X.filter[,i+1]^2/20, sd = sigma)
    X.filter[,(i+1)] <-  X.filter[sample(1:N,N,replace = TRUE, prob = weights),(i+1)]
  }
  X.filter
}



PF <- bootstrap.PF(500000,T,Y)

discret <- function(start,end,inc,mean,sd){
  #assume the grid is equally divided
  s <- seq(start,end,inc)
  prob <- s
  prob[1] <- pnorm(start+inc/2,mean=mean,sd=sd)
  prob[length(s)] <- 1-pnorm(end-inc/2,mean=mean,sd=sd)
  for (i in 2:(length(s)-1)){
    prob[i] <- pnorm(s[i]+inc/2,mean=mean,sd=sd)-pnorm(s[i]-inc/2,mean=mean,sd=sd)
  }
  cbind(s,prob)
}



#discret <- function(start,end,inc,mean,sd){
#assume the grid is equally divided
#s <- seq(start,end,inc)
##lambda <- exp(((2*mean-1)/(2*(sd^2))))
#q <- exp(-1/(sd^2))

#prob <- (lambda^s)*q^(s*(s-1)/2)
#cbind(s,renormalize(prob))

#}

inc <- 0.05
Grid <- cbind(2*floor(colMins(PF))-floor(colMaxs(PF)), 2*floor(colMaxs(PF))-1*floor(colMins(PF))   )


X.ini <- discret(Grid[1,1],Grid[1,2],inc,0,1)
X.filtering <- list(cbind(X.ini[,1],renormalize(dnorm(Y[1], X.ini[,1]^2/20, sd = sigma)*X.ini[,2])))
integral <- list(NULL)
transition.matrix <- list(NULL)

for (i in 2:T){
  print(i)
  X.ini <- seq(Grid[i,1],Grid[i,2],inc)
  int <- rep(0,length(X.ini))
  l.new <- length(X.ini)
  l.old <- nrow(X.filtering[[i-1]])
  trans.matrix <- matrix(rep(0,l.new*l.old),ncol = l.new)
  for (j in 1:l.old){
    trans.matrix[j,] <- discret(Grid[i,1],Grid[i,2],inc,0.5*X.filtering[[i-1]][j,1]+25*(X.filtering[[i-1]][j,1]/(1+X.filtering[[i-1]][j,1]^2)) + 8*cos(1.2*i),tau)[,2]
  }
  
  transition.matrix <- c(transition.matrix,list(trans.matrix))
  for (j in 1:l.new){
    int[j] <- sum(trans.matrix[,j]*X.filtering[[i-1]][,2])
  }
  integral <- c(integral,list(int))
  X.filtering <- c(X.filtering,list(cbind(X.ini,renormalize(dnorm(Y[i], X.ini^2/20, sd = sigma)*int))))
}


filtering.means <- rep(0,T)
for (i in 1:T){
  filtering.means[i] <- sum(X.filtering[[i]][,1]*X.filtering[[i]][,2])
}


X.smoothing <- X.filtering
for (i in (T-1):1){
  for (j in 1:nrow(X.filtering[[i]])){
    Sum <- X.smoothing[[i+1]][,2]*transition.matrix[[i+1]][j,]/(integral[[i+1]])
    Sum[is.nan(Sum)] <- 0
    X.smoothing[[i]][j,2] <- X.filtering[[i]][j,2]*sum(Sum)
  }
}



smoothing.means <- rep(0,T)
for (i in 1:T){
  smoothing.means[i] <- sum(X.smoothing[[i]][,1]*X.smoothing[[i]][,2])
}



save(X.filtering, X.smoothing, filtering.means, smoothing.means, file = 'NL_data51.Rdata')




