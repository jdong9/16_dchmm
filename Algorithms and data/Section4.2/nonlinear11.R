library(base)
library(RUnit)
library(matrixStats)
library(MASS)
library(lattice)
library(pscl)
library(truncnorm)
library(smcUtils)
library(coda)
library(mvtnorm)
library(mixtools)
library(xtable)
library(chopthin)
#load the data for the estimated true filtering and smoothing distribution by the discrete HMM
load('NL_data11.Rdata')
#Non-linear model
tau <- 1
sigma <- 1
gen <- function(T){
  X <- rep(0,T)
  Y <- X
  X[1] <- rnorm(1, sd = 1)
  Y[1] <- rnorm(1, X[1]^2/20, sd = sigma)
  for (i in 2:T){
    X[i] <- rnorm(1, 0.5*X[i-1] + 25*(X[i-1]/(1+X[i-1]^2)) + 8*cos(1.2*i), sd = tau)
    Y[i] <- rnorm(1, X[i]^2/20, sd = sigma)
  }
  list(X,Y)
}



#Generation of the new particles
T <- 512
set.seed(10002)
data <- gen(T)
Y <- data[[2]]
obs <- data[[1]]
num <- 500



#Kolmogorov-Smirnov Tests
N <- 50000
K <- 512
benchmark <- matrix(rep(0,N*K), ncol = K)


for (i in 1:K){
  benchmark[,i] <- X.smoothing[[i]][,1][sample(1:nrow(X.smoothing[[i]]),N,prob = X.smoothing[[i]][,2], replace = TRUE)]
}


KS_test <- function(samples, weights, benchmark){
  D <- 0
  for (i in 1:ncol(samples)){
    D <- D + ks.test(benchmark[,i], samples[sample(1:nrow(samples), nrow(samples), replace = TRUE, prob = weights),i])$statistic
  }
  D
}


## Bootstrap PF 
bootstrap.PF <- function(N,K,Y){
  X.filter <- matrix(rep(0,N*K), ncol = K)
  X.filter[,1] <- rnorm(N,mean = 0, sd = 1)
  weights <- dnorm(Y[1], mean = X.filter[,1]^2/20, sd = sigma)
  X.filter[,1] <- X.filter[sample(1:N,N,replace = TRUE, prob = weights),1]
  for (i in 1:(K-1)){
    X.filter[,(i+1)] <- rnorm(N, mean = 0.5*X.filter[,i] + 25*X.filter[,i]/(1+X.filter[,i]^2) + 8*cos(1.2*(i+1)), sd = tau)
    weights <- dnorm(Y[i+1], mean = X.filter[,i+1]^2/20, sd = sigma)
    X.filter[,(i+1)] <-  X.filter[sample(1:N,N,replace = TRUE, prob = weights),(i+1)]
  }
  X.filter
}



## Bootstrap PS 
bootstrap.PS <- function(N,K,Y){
  ancestor <- matrix(rep(0,N*K), ncol = K)
  X.filter <- matrix(rep(0,N*K), ncol = K)
  X.smoother <- X.filter
  X.filter[,1] <- rnorm(N,mean = 0, sd = 1)
  weights <- dnorm(Y[1], mean = X.filter[,1]^2/20, sd = sigma)
  resample <- sample(1:N,N,replace = TRUE, prob = weights)
  ancestor[,1] <- resample
  X.filter[,1] <- X.filter[resample,1]
  
  for (i in 1:(K-1)){
    X.filter[,(i+1)] <- rnorm(N, mean = 0.5*X.filter[,i] + 25*X.filter[,i]/(1+X.filter[,i]^2) + 8*cos(1.2*(i+1)), sd = tau)
    weights <- dnorm(Y[i+1], mean = X.filter[,i+1]^2/20, sd = sigma)
    resample <- sample(1:N,N,replace = TRUE, prob = weights)
    ancestor[,i+1] <- resample
    X.filter[,(i+1)] <- X.filter[resample,(i+1)]
  }
  
  X.smoother[,K] <- X.filter[,K]
  pos <- 1:N
  for (j in K:2){
    #pos <- ancestor[pos,j]
    pos <- ancestor[pos,j]
    X.smoother[,(j-1)] <- X.filter[pos,(j-1)]
  }
  
  X.smoother
}


#simulation of BPF
X.bootstrap.means <- matrix(rep(0,num*T), nrow = num)
X.bootstrap.vars <- X.bootstrap.means
D.bootstrap <- rep(0,num)

set.seed(11113)
timePS <- system.time(
  for (i in 1:num){
    #print(i)
    a <- bootstrap.PS(40000,T,Y)
    X.bootstrap.means[i,] <- colMeans(a)
    X.bootstrap.vars[i,] <- colVars(a)
    D.bootstrap[i] <- KS_test(a, rep(1,40000), benchmark)
  }
)


#FFBSm
FFBSm <- function(N,Y){
  T <- length(Y)
  X.FFBSm <- bootstrap.PF(N,T,Y)
  for (i in (T-1):1){
    denorm <- rep(0,N)
    for (j in 1:N){
      denorm[j] <- sum(dnorm(X.FFBSm[j,i+1], 0.5*X.FFBSm[,i] + 25*(X.FFBSm[,i]/(1+X.FFBSm[,i]^2)) + 8*cos(1.2*(i+1)), sd = tau))
    }
    weights <- rep(0,N)
    for (j in 1:N){
      weights[j] <- sum(   dnorm(X.FFBSm[,i+1], 0.5*X.FFBSm[j,i] + 25*(X.FFBSm[j,i]/(1+X.FFBSm[j,i]^2)) + 8*cos(1.2*(i+1)), sd = tau)/denorm  )
    }
    X.FFBSm[,i] <- X.FFBSm[sample(1:N,N,prob = weights, replace = TRUE),i]
  }
  return(X.FFBSm)
}


#simulation of FFBSm
X.FFBSm.means <- matrix(rep(0,num*T), nrow = num)
X.FFBSm.vars <- X.FFBSm.means
D.FFBSm <- rep(0,num)

set.seed(22224)
timeFFBSm <- system.time(
  for (i in 1:num){
    a <- FFBSm(315,Y)
    X.FFBSm.means[i,] <- colMeans(a)
    X.FFBSm.vars[i,] <- colVars(a)
    D.FFBSm[i] <- KS_test(a,rep(1,315),benchmark)
  }
)


#FFBSi
FFBSa <- function(N,Y){
  T <- length(Y)
  X.SIS <- bootstrap.PF(N,T,Y)
  X.FFBSa <- matrix(rep(0,T*N), ncol = T)
  X.FFBSa[,T] <- X.SIS[sample(1:N,N,replace = TRUE),T]
  for (j in 1:N){
    X.FFBSa[j,T] <- X.SIS[sample(1:N,1,prob = rep(1/N,N),replace = TRUE),T]
    for (i in (T-1):1){
      weights <- dnorm(X.FFBSa[j,i+1], mean = 0.5*X.SIS[,i] + 25*X.SIS[,i]/(1+X.SIS[,i]^2) + 8*cos(1.2*(i+1)), sd = tau) 
      X.FFBSa[j,i] <-  X.SIS[sample(1:N,1,prob = weights, replace = TRUE),i]
    }
  }
  X.FFBSa
}

#simulation of FFBSi
X.FFBSa.means <- matrix(rep(0,num*T), nrow = num)
X.FFBSa.vars <- X.FFBSa.means
D.FFBSa <- rep(0,num)
set.seed(33335)
for (i in 1:num){
  #print(i)
  a <- FFBSa(320,Y)
  X.FFBSa.means[i,] <- colMeans(a)
  X.FFBSa.vars[i,] <- colVars(a)
  D.FFBSa[i] <- KS_test(a,rep(1,320),benchmark)
}


#TPS-EFP
f <- function(x,cd){
  xnew <- x
  cdnew <- cd
  function(N){approxfun(cdnew, xnew, yleft = -Inf, yright = Inf, tie = "ordered")(runif(N))}
}


combined.sampling2.stepfun1run <- function(X1,X2,w1,w2,n1,n2,index,Y){
  n <- nrow(X1)  
  X <- cbind(X1,X2)  
  N <- ncol(X1)
  
  #if (k!=1){
  #s1 <- chopthin(w1,n*k)
  #s2 <- chopthin(w2,n*k)
  
  #X1 <- as.matrix(X1[s1[[2]],])
  #w1 <- s1[[1]]
  #n1 <- n1[s1[[2]]]
  #X2 <- as.matrix(X2[s2[[2]],])
  #w2 <- s2[[1]]
  #n2 <- n2[s2[[2]]]
  #}
  #shuffle <- sample(1:(n*k),(n*k))
  #X1 <- as.matrix(X1[shuffle,])
  #w1 <- w1[shuffle]
  #n1 <- n1[shuffle]
  
  
  #res1 <- sample(1:n,n, replace = TRUE, prob = w1)
  #res2 <- sample(1:n,n, replace = TRUE, prob = w2)
  #X1 <- as.matrix(X1[res1,])
  #X2 <- as.matrix(X2[res2,])
  #w1 <- rep(1,n)
  #w2 <- rep(1,n)
  #n1 <- n1[res1]
  #n2 <- n2[res2]
  
  
  samples <- cbind(X1,X2)  
  if (length(index) != length(Y)){
    weights <- w1*w2*dnorm(samples[,N+1], mean = 0.5*samples[,N] + 25*samples[,N]/(1+samples[,N]^2) + 8*cos(1.2*index[N+1]), sd = tau) * dnorm(Y[index[N+1]], mean = samples[,N+1]^2/20, sd = sigma)/n2}
  if (length(index) == length(Y)){
    weights <- w1*w2*dnorm(Y[1], mean = samples[,1]^2/20, sd = sigma) * dnorm(samples[,N+1], mean = 0.5*samples[,N] + 25*samples[,N]/(1+samples[,N]^2) + 8*cos(1.2*index[N+1]), sd = tau) * dnorm(Y[index[N+1]], mean = samples[,N+1]^2/20, sd = sigma)/n2}
  
  #w <- (sum((weights/sum(weights))^2))^(-1)
  #sample.ct <- chopthin(weights,n)
  #n1 <- n1[sample.ct[[2]]]
  choose <- sample(1:n,n,prob = weights, replace = TRUE)
  weights <- rep(1,n)
  list(samples[choose,],renormalize(weights),n1[choose])
  #list(samples[sample.ct[[2]],],sample.ct[[1]]/sum(sample.ct[[1]]),n1)
}



tree.sampling2.stepfun1run <- function(X,Y){
  level <- ceiling(log2(length(X)))
  n <- length(X[[1]][[2]])
  parents <- X
  tree <- parents
  for (i in 1:level){
    children <- rep(list(0),  ceiling((length(parents)/2))    )
    for (j in 1:    floor((length(parents)/2))){
      index <- c(parents[[2*j-1]][[1]],parents[[2*j]][[1]])
      result <- combined.sampling2.stepfun1run(as.matrix(parents[[2*j-1]][[2]]),as.matrix(parents[[2*j]][[2]]),as.matrix(parents[[2*j-1]][[3]]),as.matrix(parents[[2*j]][[3]]),as.matrix(parents[[2*j-1]][[4]]),as.matrix(parents[[2*j]][[4]]),index,Y)
      #print(result[[5]])
      #children[[j]] <- list(index, result[[1]], result[[2]],result[[3]],result[[4]],result[[5]])
      children[[j]] <- list(index, result[[1]], result[[2]],result[[3]])
    }
    if ( ceiling((length(parents)/2)) > (length(parents)/2)  ){
      children[[ceiling((length(parents)/2))]] <- list( parents[[   2*ceiling((length(parents)/2))-1]][[1]],parents[[   2*ceiling((length(parents)/2))-1]][[2]], NA)
    }
    parents <- children
    if (i == 1){
      tree <- list(children, tree)}
    if (i!=1){
      tree <- c(list(children),tree)
    }
  }
  tree
}





tree2.sampling.stepfun1run <- function(n,N,Y){
  T <- length(Y)
  PF <- bootstrap.PF(n,T,Y)
  X.2 <- rep(list(NULL), T)
  X.2[[1]] <- list(1,matrix(rnorm(N,0,1)),rep(1/N,N),rep(1,N))
  
  for (j in 2:T){
    #print(j)
    mat <- cbind(sort(PF[,j]),rep(1/n,n))
    
    ker <- density(mat[,1],weights = mat[,2])
    ker$y[which(ker$y == 0)] <- min(ker$y[which(ker$y != 0)])
    pd <- 0.5*(ker$y[-1] + ker$y[-length(ker$y)])
    cd <- c(0,cumsum(pd))/sum(pd)
    pd <- c(0,pd,0)
    piecewise_funcs_PF <- list(cdf=approxfun(ker$x,cd,yleft=0,yright=1,tie = "ordered"),q=approxfun(cd,ker$x,yleft=-Inf,yright=Inf,tie = "ordered"),pdf=stepfun(ker$x,pd), r = f(x = ker$x, cd = cd))
    
    samples <- piecewise_funcs_PF$r(N)
    X.2[[j]] <- list(j,samples,rep(1,N),piecewise_funcs_PF$pdf(samples))
  }
  
  tree2 <- tree.sampling2.stepfun1run(X.2,Y)
  tree2
}


#simulation of TPS-EFP
X.tree.step1run.means <- matrix(rep(0,num*T), nrow = num)
D.tree.step1run <- rep(0,num)
set.seed(66668)
for (i in 1:num){
  #print(i)
  a <- tree2.sampling.stepfun1run(10000,10000,Y)[[1]][[1]]
  X.tree.step1run.means[i,] <- colSums(a[[2]]*a[[3]])
  D.tree.step1run[i] <- KS_test(a[[2]],a[[3]],benchmark)
}





#TPS-L
combined.sampling1 <- function(X1,X2,k,index,Y){
  n <- nrow(X1)  
  X <- cbind(X1,X2)  
  N <- ncol(X1)
  
  #s1 <- sample(1:n, (n*k), replace = TRUE, prob = rep(1/n,n))
  #s2 <- sample(1:n, (n*k), replace = TRUE, prob = rep(1/n,n))
  
  #X1 <- as.matrix(X1[s1,])
  #X2 <- as.matrix(X2[s2,])
  
  samples <- cbind(X1,X2)
  
  weights <- dnorm(samples[,N+1], mean = 0.5*samples[,N] + 25*samples[,N]/(1+samples[,N]^2) + 8*cos(1.2*index[N+1]), sd = tau) 
  #for (i in 1:(n*k)){
  #mRess1[s1[i]] <- mRess1[s1[i]] + weights[i]
  #mRess2[s2[i]] <- mRess2[s2[i]] + weights[i]
  #}
  weights <- weights/sum(weights)
  w <- (sum((weights)^2))^(-1)
  
  choose <- sample(1:(k*n),n,replace = TRUE, prob = weights)
  weights <- rep(1/n,n)
  #list(samples[choose,],samples, weights, w/(n*k), (sum((table(choose)/n)^2))^(-1)/(n), c( (sum    (    (mRess1/sum(mRess1))    ^2))^(-1)/n, (sum    (    (mRess2/sum(mRess2))    ^2))^(-1)/n  ))
  list(as.matrix(samples[choose,]),weights, samples, w/(n*k),(sum((table(choose)/n)^2))^(-1)/(n))
}


tree.sampling1 <- function(X,k,Y){
  level <- ceiling(log2(length(X)))
  n <- length(X[[1]][[2]])
  parents <- X
  tree <- parents
  for (i in 1:level){
    children <- rep(list(0),  ceiling((length(parents)/2))    )
    for (j in 1:floor((length(parents)/2))){
      index <- c(parents[[2*j-1]][[1]],parents[[2*j]][[1]])
      result <-  combined.sampling1(as.matrix(parents[[2*j-1]][[2]]),as.matrix(parents[[2*j]][[2]]),k,index,Y)
      #children[[j]] <- list(index, result[[1]], result[[2]],result[[3]],result[[4]],result[[5]],result[[6]])
      children[[j]] <- list(index, result[[1]], result[[2]],result[[3]],result[[4]],result[[5]])
    }
    if ( ceiling((length(parents)/2)) > (length(parents)/2)  ){
      children[[ceiling((length(parents)/2))]] <- list( parents[[2*ceiling((length(parents)/2))-1]][[1]],parents[[2*ceiling((length(parents)/2))-1]][[2]], NA)
    }
    parents <- children
    if (i == 1){
      tree <- list(children, tree)}
    if (i !=1){
      tree <- c(list(children),tree)
    }
  }
  tree
}



tree1.sampling.1 <- function(N,T,Y,k){
  X.1 <- rep(list(NULL), T)
  
  X.1[[1]][[2]] <- rnorm(N,mean = 0, sd = 1)
  weights <- dnorm(Y[1], mean = X.1[[1]][[2]]^2/20, sd = sigma)
  X.1[[1]][[2]] <- X.1[[1]][[2]][sample(1:N,N,replace = TRUE, prob = weights)]
  X.1[[1]] <- list(1,X.1[[1]][[2]])
  
  
  for (i in 2:length(Y)){
    X.1[[i]] <- list(i,matrix(  sample(c(-1,1),N,replace = TRUE) * sqrt(qnorm(runif(N,pnorm(0,20*Y[i],20*sigma),1),20*Y[i],20*sigma))   ))
  }
  
  #rtruncnorm(N, a=0, b=Inf, mean = 20*Y[i], sd = 20*sigma)
  
  tree1 <- tree.sampling1(X.1,1,Y)
}

#simulation of TPS-L
set.seed(77779)
X.tree1.1.means <- matrix(rep(0,num*T), nrow = num)
X.tree1.1.vars <- X.tree1.1.means
D.tree1 <- rep(0,num)
timeTree1.1 <- system.time(
  for (i in 1:num){
    #print(i)
    a <- tree1.sampling.1(13000,T,Y,1)
    X.tree1.1.means[i,] <- colMeans(a[[1]][[1]][[2]])
    X.tree1.1.vars[i,] <- colVars(a[[1]][[1]][[2]])
    D.tree1[i] <- KS_test(a[[1]][[1]][[2]],a[[1]][[1]][[3]],benchmark)
  }
)


#MSE of mean
MSE.means <- matrix(rep(0,10),ncol=2)
MSE.means[1,] <- c(mean(rowMeans((X.bootstrap.means - matrix(rep(smoothing.means,num),nrow = num, byrow = TRUE))^2)),sd(rowMeans((X.bootstrap.means- matrix(rep(obs,num),nrow = num, byrow = TRUE))^2))/sqrt(num))
MSE.means[2,] <- c(mean(rowMeans((X.FFBSm.means - matrix(rep(smoothing.means,num),nrow = num, byrow = TRUE))^2)),sd(rowMeans((X.FFBSm.means- matrix(rep(obs,num),nrow = num, byrow = TRUE))^2))/sqrt(num))
MSE.means[3,] <- c(mean(rowMeans((X.FFBSa.means - matrix(rep(smoothing.means,num),nrow = num, byrow = TRUE))^2)),sd(rowMeans((X.FFBSa.means- matrix(rep(obs,num),nrow = num, byrow = TRUE))^2))/sqrt(num))
MSE.means[4,] <- c(mean(rowMeans((X.tree.step1run.means - matrix(rep(smoothing.means,num),nrow = num, byrow = TRUE))^2)),sd(rowMeans((X.tree.step1run.means - matrix(rep(smoothing.means,num),nrow = num, byrow = TRUE))^2))/sqrt(num))
MSE.means[5,] <- c(mean(rowMeans((X.tree1.1.means - matrix(rep(smoothing.means,num),nrow = num, byrow = TRUE))^2)),sd(rowMeans((X.tree1.1.means - matrix(rep(smoothing.means,num),nrow = num, byrow = TRUE))^2))/sqrt(num))


D <- rep(0,5)
D[1] <- mean(D.bootstrap)
D[2] <- mean(D.FFBSm)
D[3] <- mean(D.FFBSa)
D[4] <- mean(D.tree.step1run)
D[5] <- mean(D.tree1)

N <- c(40000,315,320,10000,13000)
n <- c(NA,315,320,10000,NA)
MSE <- cbind(N,n,MSE.means,D)
colnames(MSE) <- c('N','n','Mean of MSEm','s.d. of MSEm','Mean of KSm')
rownames(MSE) <- c('BPF','FFBSm','FFBSi','TPS-EFP','TPS-L')

table <- xtable(MSE)
digits(table) <- c(0,0,0,4,5,2)
print(table)

save(MSE,table,X.bootstrap.means,X.FFBSm.means,X.FFBSa.means,X.tree.step1run.means,D,file="nonlinear_simulation11.Rdata")
